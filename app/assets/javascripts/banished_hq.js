var BanishedHQ;

BanishedHQ = {
  pageLoad: function(onLoad) {
    /*
     * According to the Turbolinks docs, the event 'page:change' is
     * called on the initial full request, as well as subsequent
     * requests through Turbolinks.  This would indicate we could
     * get away with this...
     *   $(document).on('page:change', onLoad);
     *
     * However, that seems to mess up back-button behavior.  The
     * back-button (at least in chrome) will trigger the 'page:change'
     * event as well.  any jQuery.on() call then breaks.  One way
     * to fix that is to user jQuery.off().on() everywhere, which
     * feels like a kludge.
     *
     * This code gets jQuery to run out initialization code on
     * document ready, and then let Turbolinks run the same code
     * after 'page:load'.
     */
    jQuery(onLoad);
    return $(document).on('page:load', onLoad);
  },

  components: {},
  viewSpecific: {}
};
