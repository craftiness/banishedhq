//= require jquery
//= require best_in_place
//= require jquery_ujs
//= require turbolinks
//= require banished_hq
//= require foundation

BanishedHQ.pageLoad(function(){
  $(document).foundation();
  $(".best_in_place").best_in_place();
});

BanishedHQ.pageLoad(function() {
  $('.plant-seed input[type="checkbox"]').on('change', function(e) {
    var $self = $(this);
    $self.closest('.plant-seed').toggleClass('changed');
  });
});
