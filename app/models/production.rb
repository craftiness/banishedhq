class Production
  include ActiveModel::Model

  attr_accessor :id, :inputs, :outputs, :building

  validates :id, :outputs, presence: true

  ALL = []

  data_file = File.expand_path('../productions.json', __FILE__)
  JSON::parse(File.read(data_file)).tap do |json|
    json['production_maps'].each do |data|
      outputs = data['outputs'].map { |name|
        Product.find_by_name!(name)
      }

      data['input_options'].each do |input|
        inputs = input['inputs'].map { |name|
          Product.find_by_name!(name)
        }

        ALL << Production.new(
                          id: input['id'],
                      inputs: inputs.freeze,
                     outputs: outputs.dup.freeze)
      end
    end
  end

  def self.consuming(product)
    ALL.select { |p| p.inputs.include?(product) }
  end

  def self.producing(product)
    ALL.select { |p| p.outputs.include?(product) }
  end
end
