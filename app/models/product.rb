class Product
  include ActiveModel::Model

  attr_accessor :id, :name

  def self.build(id, name)
    Product.new(id: id, name: name)
  end

  ALL = [
    build( 1, 'Ale'),
    build( 2, 'Cloth'),
    build( 3, 'Fire Bundle'),
    build( 4, 'Flour'),
    build( 5, 'Fruit Bread'),
    build( 6, 'Linen'),
    build( 7, 'Liquor'),
    build( 8, 'Maple Sap'),
    build( 9, 'Nut Bread'),
    build(10, 'Oil'),
    build(11, 'Pipe Tobacco'),
    build(12, 'Rope'),
    build(13, 'Silkworms'),
    build(14, 'Sugar'),
    build(15, 'Wine'),
    build(16, 'Apples'),
    build(17, 'Apricots'),
    build(18, 'Bamboo'),
    build(19, 'Barley'),
    build(20, 'Beans'),
    build(21, 'Beetroots'),
    build(22, 'Blackberries'),
    build(23, 'Blueberries'),
    build(24, 'Brocolli'),
    build(25, 'Cabbage'),
    build(26, 'Carrots'),
    build(27, 'Cherries'),
    build(28, 'Chestnuts'),
    build(29, 'Chickpeas'),
    build(30, 'Corn'),
    build(31, 'Cotton'),
    build(32, 'Cucumbers'),
    build(33, 'Figs'),
    build(34, 'Flax'),
    build(35, 'Grapes'),
    build(36, 'Hemp'),
    build(37, 'Kale'),
    build(38, 'Kernels'),
    build(39, 'Lettuce'),
    build(40, 'Maple Sap'),
    build(41, 'Olives'),
    build(42, 'Parsnips'),
    build(43, 'Peaches'),
    build(44, 'Pears'),
    build(45, 'Peas'),
    build(46, 'Pecans'),
    build(47, 'Peppers'),
    build(48, 'Plums'),
    build(49, 'Potatoes'),
    build(50, 'Pumpkins'),
    build(51, 'Quinces'),
    build(52, 'Radishes'),
    build(53, 'Raspberries'),
    build(54, 'Red Mulberries'),
    build(55, 'Sorghum'),
    build(56, 'Soybeans'),
    build(57, 'Spinach'),
    build(58, 'Squash'),
    build(59, 'Strawberries'),
    build(60, 'Sugar Cane'),
    build(61, 'Tobacco Leaf'),
    build(62, 'Tomatoes'),
    build(63, 'Turnips'),
    build(64, 'Walnuts'),
    build(65, 'Watermelons'),
    build(66, 'Wheat'),
    build(67, 'White Mulberry Leaves'),
    build(68, 'Maple Syrup'),
  ].freeze

  def self.count
    ALL.size
  end

  def self.find_by_name(name)
    ALL.detect { |p| p.name.downcase == name.to_s.downcase }
  end

  def self.find_by_name!(name)
    find_by_name(name) or raise "Record not found for #{name}"
  end

  def consumed_in
    Production.consuming(self)
  end

  def used_for
    consumed_in.map(&:outputs).flatten
  end

  def to_s
    name
  end
end
