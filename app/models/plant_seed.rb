class PlantSeed
  include ActiveModel::Model

  attr_accessor :id, :name, :plant_type, :from_mod_id, :product,
    :maturation_speed, :frost_resistance

  MODS = {
    1 => 'Vanilla Banished',
    2 => 'Colonial Charter',
  }.freeze
  MOD_OPTIONS = MODS.invert.freeze

  validates :product, presence: true
  validates :plant_type,  inclusion: { in: %w[ crop orchard ] }
  validates :from_mod_id, inclusion: { in: MODS.keys }

  validates :name, presence: true

  ALL = []

  data_file = File.expand_path('../plant_seeds.json', __FILE__)
  JSON::parse(File.read(data_file)).tap do |json|
    json['plant_seeds'].each do |data|
      data['product'] = Product.find_by_name(data.delete('product_name'))
      plant_seed = PlantSeed.new(data)
      raise 'Invalid PlantSeed' unless plant_seed.valid?
      ALL << plant_seed
    end
  end

  def orchard?
    plant_type == 'orchard'
  end

  def self.all
    ALL
  end

  def self.count
    all.size
  end

  def to_s
    name
  end

end
