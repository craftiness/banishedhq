class Building
  include ActiveModel::Model

  attr_accessor :id, :name # ...

  validates :name, presence: true

  ALL = []

  def self.count
    ALL.size
  end

end
