class Map < ActiveRecord::Base
  belongs_to :user

  before_validation :nilify_blanks

  scope :most_recent, -> { order('maps.updated_at') }

  def plant_seeds
    PlantSeed.all
  end

  def unacquired_plant_seeds
    PlantSeed.all.reject { |ps| has_seed? ps }
  end

  def acquired_plant_seeds
    PlantSeed.all.select { |ps| has_seed? ps }
  end

  def has_seed?(seed)
    acquired_seeds.include?(seed.id)
  end

  private
  def nilify_blanks
    self.name = nil if self.name.blank?
  end
end
