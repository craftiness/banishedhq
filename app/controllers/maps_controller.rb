class MapsController < ApplicationController

  before_action :find_map, except: [ :index, :new, :create ]

  def index
    @maps = current_user.maps.most_recent
  end

  def show
  end

  def new
    @map = Map.new
  end

  def create
    @map = current_user.maps.build(permitted_params)
    if @map.save
      redirect_to @map
    else
      render action: :new
    end
  end

  def update
    if @map.update(permitted_params)
      redirect_to @map
    else
      render action: :show
    end
  end

  def destroy
    @map.destroy
    redirect_to action: :index
  end

  private

  def find_map
    @map = current_user.maps.find(params[:id])
  end

  def permitted_params
    params.require(:map).permit(
      :name, acquired_seeds: [])
  end

end
