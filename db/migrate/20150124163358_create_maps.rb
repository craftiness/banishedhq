class CreateMaps < ActiveRecord::Migration
  def change
    create_table :maps do |t|
      t.references :user, null: false, index: true
      t.string     :name
      t.integer    :acquired_seeds, array: true, default: []

      t.timestamps null: false
    end
  end
end
