#!/usr/bin/env ruby

area_per_animal = {
  'cows' => 20,
  'sheep' => 16,
  'chickens' => 6,
}

animal = ARGV.shift or raise "Must provide animal"
area   = area_per_animal[animal] or
  raise "Must provide one of #{area_per_animal.keys.inspect}"

edge = ARGV.shift or raise "Must provide one dimension"
edge = Integer(edge)

puts (1..60)
  .map { |i| i * edge }
  .select { |a| a % area == 0 }
  .map { |w| w / edge }
  .map { |h| "%2d X %2d => %3d" % [edge, h, (edge*h/area)] }
