require 'rails_helper'

RSpec.describe Production, type: :model do

  it 'has several productions for Ale' do
    ale = Product.find_by_name('Ale')
    expect(Production.producing(ale).count).to eq 5
  end

  it 'has two inputs for Fruit Bread' do
    fb = Product.find_by_name('Fruit Bread')
    expect(Production.producing(fb).count).to eq 1
    expect(Production.producing(fb).first.inputs.size)
      .to eq 2
  end

end
