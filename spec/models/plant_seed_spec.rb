require 'rails_helper'

RSpec.describe PlantSeed, :type => :model do
  it 'has lots of entries' do
    expect(PlantSeed.count).to eq 52
  end
end
