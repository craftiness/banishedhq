require 'rails_helper'

RSpec.describe Product, type: :model do

  it 'can be found by name' do
    expect(Product.find_by_name('Flour')).to eq Product::ALL[3]
  end

end
