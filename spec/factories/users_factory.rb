FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "test-user-#{n}@test.host" }
    password         'password'
  end
end
